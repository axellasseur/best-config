#!/bin/bash

if [[ `pwd` != *"best-config" ]]; then
	echo "You must be in the best-config directory to launch this installer :)"
	exit
fi

#disclaimer

echo "Hey ! I'm Axel ⊂(◉‿◉)つ"
echo -e "It seems that you look for an awesome configuration for your ~/.init directory\n"
echo -e "(⌐▨_▨)\nthis installer will copy the contents below to your .init directory : \n"

for DIRECTORY in *; do
	if [ -d $DIRECTORY ]; then
		echo "- ${DIRECTORY}"
	fi
done

while true; do
	echo -e "\nDo you wanna install it ? [y/n]"
	read -r ANSWER
	if [ $ANSWER == 'y' ]; then

		#personal notes
		echo -e "\nHey ⊂(◉‿◉)つ wait a sec !\n\nTheses are my personal configurations that I use regularly, and I gonna probably update it as well. So, if you see a bulshit or some personal leaks, please send me a message, don't betray a friend (•﹏•)\n"
		echo "Well, press enter to continue"
		read -r ANSWER
		echo -e "(∩｀-´)⊃━☆ﾟ.*･｡ﾟ\n"

		#installation
		for DIRECTORY in *; do
			if [ -d $DIRECTORY ]; then
				echo "ᕕ(ᐛ)ᕗ cp -r ${DIRECTORY} ~/.config/"
				cp -r $DIRECTORY ~/.config/
			fi
		done

		echo -e "\nok, it's done (•̀ᴗ•́)و"
		exit
	elif [ $ANSWER == 'n' ]; then
		echo "ok, installation aborted :p"
		exit
	else
		echo "please answer by 'y' (yes) or 'n' (no), then press enter, don't be dummy"
	fi	
done

echo $answer

echo "Theses are my personal configurations that I use regularly, and I gonna probably update it as well."
echo "So, if you see a bulshit or some personal leaks, please send me a message, don't betray a friend :)"
