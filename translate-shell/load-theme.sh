#!/bin/bash
FILENAME=$(basename $1)
if [ ! -f $FILENAME ]
then
	wget "$1"
fi
if [ "${FILENAME##*.}" = "trans" ]
then
	echo $FILENAME
	sed -i "s/\(:theme *\)\"[a-z.]*\"/\1\"$FILENAME\"/" init.trans
else
	echo "Wrong file name"
fi
