# My best config for ~/.config file#

git clone this repository, then feel free to copy/paste the config directories you want in your `~/.config`

`cd ~/.config && git clone https://axellasseur@bitbucket.org/axellasseur/best-config.git`

If you want everything without thinking about it, simply run `./install.sh` (don't worry, there is a disclaimer before anything happens in your `~/.init`, it's safe)

Good vibrations
